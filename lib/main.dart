import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'core/localization/app_translations_delegate.dart';
import 'core/localization/application.dart';
import 'features/presentation/providers/HomeProvider.dart';
import 'features/presentation/providers/NewsListProvider.dart';
import 'features/presentation/providers/UsersProvider.dart';
import 'features/presentation/screens/MainScreens/details_screen.dart';
import 'features/presentation/screens/MainScreens/full_stats_screen.dart';
import 'features/presentation/screens/MainScreens/home_screen.dart';
import 'features/presentation/screens/MainScreens/league_table_screen.dart';
import 'features/presentation/screens/main_screen.dart';
import 'features/presentation/screens/MainScreens/more_screen.dart';
import 'features/presentation/screens/MainScreens/news_list_screen.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  return runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //////////Language/////////
  AppTranslationsDelegate _newLocaleDelegate;
  String lang = 'ar';

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }

  final supportedLocales = [
    const Locale("en", ""),
    const Locale("ar", ""),
  ];

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        // Providers Models
        providers: [
          ChangeNotifierProvider(
            create: (_) => NewsListProvider(),
          ),
          ChangeNotifierProvider(
            create: (_) => UsersProvider(),
          ),
          ChangeNotifierProvider(
            create: (_) => HomeProvider(),
          )
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            fontFamily: 'Cairo'
          ),

          // language
          localizationsDelegates: [
            GlobalCupertinoLocalizations.delegate,
            _newLocaleDelegate,
            //provides localised strings
            GlobalMaterialLocalizations.delegate,
            //provides RTL support
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: supportedLocales,
          locale: Locale(lang, ''),

          // init screens
          initialRoute: MainScreen.routeName,
          routes: {
            MainScreen.routeName: (_) => MainScreen(),
            HomeScreen.routeName: (_) => HomeScreen(),
            FullStatsScreen.routeName: (_) => FullStatsScreen(),
            LeagueTableScreen.routeName: (_) => LeagueTableScreen(),
            NewsListScreen.routeName: (_) => NewsListScreen(),
            MoreScreen.routeName: (_) => MoreScreen(),
            DetailsScreen.routeName: (_) => DetailsScreen()
          },
        ));
  }
}
