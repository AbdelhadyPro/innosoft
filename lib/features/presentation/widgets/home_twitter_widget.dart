import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeTwitterWidget extends StatelessWidget {
  final String image;
  final String title;
  final String accout;
  final String content;

  const HomeTwitterWidget(
      {Key key, this.image, this.title, this.accout, this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.all(screenSize.width * 2 / 100),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.asset(
                image,
                height: screenSize.height * 5 / 100,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    title,
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: screenSize.height * 2.5 / 100),
                  ),
                  Text(
                    accout,style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.w600,
                      fontSize: screenSize.height * 2 / 100),
                  ),
                ],
              )
            ],
          ),
          Container(
            width: screenSize.width * 90/100,
            child: Text(
              content,
              maxLines: 3,
              textAlign: TextAlign.start,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
}
