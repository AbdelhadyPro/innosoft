import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UsersWidget extends StatelessWidget {
  final String image;
  final String email;
  final String firstname;
  final String lastname;

  const UsersWidget(
      {Key key, this.image, this.email, this.firstname, this.lastname})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return Card(
      margin: EdgeInsets.only(
          top: screenSize.width * 3 / 100,
          left: screenSize.width * 3 / 100,
          right: screenSize.width * 3 / 100),
      child: Row(
        children: <Widget>[
          Container(
            width: screenSize.width * 20 / 100,
            margin: EdgeInsets.all(screenSize.width * 2 / 100),
            child: ClipOval(
              child: Image.network(
                image,
                width: screenSize.width * 20 / 100,
              ),
            ),
          ),
          Container(
            width: screenSize.width * 66 / 100,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  firstname + ' ' + lastname,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: screenSize.height * 3 / 100),
                ),
                Text(email),
              ],
            ),
          )
        ],
      ),
    );
  }
}
