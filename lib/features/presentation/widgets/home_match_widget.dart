import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeMatchWidget extends StatelessWidget {
  final String firstTeam;
  final String secondTeam;
  final String time;
  final String date;

  const HomeMatchWidget(
      {Key key, this.firstTeam, this.secondTeam, this.time, this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: screenSize.width * 2.5 / 100),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset('assets/images/team_image.png',height: screenSize.height * 7/100,),
          SizedBox(width: screenSize.width * 2/100,),
          Text(firstTeam,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize:screenSize.height * 2/100),),
          Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(time,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold,fontSize:screenSize.height * 2/100)),
              Text(date,style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold,fontSize:screenSize.height * 2/100)),
            ],
          ),
          Spacer(),
          Text(secondTeam,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize:screenSize.height * 2/100 )),
          SizedBox(width: screenSize.width * 2/100,),
          Image.asset('assets/images/team_image.png',height: screenSize.height * 7/100,),
        ],
      ),
    );
  }
}
