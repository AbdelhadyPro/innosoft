import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MoreBtnWidget extends StatelessWidget {
  final screenSize;
  final String title;
  final Color color;
  final Function onClick;

  const MoreBtnWidget(
      {Key key, this.screenSize, this.title, this.color, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onClick,
      padding: EdgeInsets.all(0),
      child: Container(
        child: Row(
          children: <Widget>[
            Container(
              margin:
                  EdgeInsets.symmetric(vertical: screenSize.height * 0.5 / 100),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5), color: color),
              width: 2,
              height: screenSize.height * 6 / 100,
            ),
            SizedBox(
              width: screenSize.width * 6.5 / 100,
            ),
            Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                  fontSize: screenSize.height * 2.8 / 100),
            )
          ],
        ),
      ),
    );
  }
}
