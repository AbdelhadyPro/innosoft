import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeVideosWidget extends StatelessWidget {
  final String image;
  final String title;

  const HomeVideosWidget({Key key, this.image, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          // Image
          Image.asset(
            image,
            width: screenSize.width * 95 / 100,
            height: screenSize.height * 35 / 100,
            fit: BoxFit.cover,
          ),

          Image.asset(
            'assets/images/play.png',
            width: screenSize.width * 35 / 100,
            height: screenSize.width * 35 / 100,
            fit: BoxFit.cover,
          ),

          // layer 1
          Positioned(
              right: screenSize.height * 5.5 / 100,
              bottom: screenSize.height * 2.5 / 100,
              child: Image.asset('assets/images/video_hint_shape.png',
                  fit: BoxFit.fill,
                  color: Colors.black,
                  width: screenSize.width * 72 / 100,
                  height: screenSize.width * 12 / 100)),

          // layer 2
          Positioned(
              right: screenSize.height * 6 / 100,
              bottom: screenSize.height * 3 / 100,
              child: Image.asset('assets/images/video_hint_shape.png',
                  fit: BoxFit.fill,
                  width: screenSize.width * 70 / 100,
                  height: screenSize.width * 11 / 100)),
          // title
          Positioned(
              right: screenSize.height * 10 / 100,
              bottom: screenSize.height * 2.5 / 100,
              child: Container(
                  child: Text(
                    title,
                    style: TextStyle(color: Colors.black87,fontSize: screenSize.height * 2/100,fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  width: screenSize.width * 60 / 100,
                  height: screenSize.width * 11 / 100)),

          //team logo
          Positioned(
              right: screenSize.height * 1 / 100,
              bottom: screenSize.height * 2 / 100,
              child: Image.asset('assets/images/team_image.png',
                  width: screenSize.width * 18 / 100,
                  height: screenSize.width * 18 / 100))
        ],
      ),
    );
  }
}
