import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeWinnerWidget extends StatelessWidget {
  final String image;
  final String title;
  final String percent;

  const HomeWinnerWidget({Key key, this.image, this.title, this.percent})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return FlatButton(
        padding: EdgeInsets.all(0),
    onPressed: () {},
    child:Container(
      width: screenSize.width * 30 / 100,
      height: screenSize.height * 35 / 100,
      padding: EdgeInsets.all(screenSize.width * 2 / 100),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: screenSize.width * 8 / 100,
                vertical: screenSize.height * 3 / 100),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1),
                borderRadius: BorderRadius.circular(5)),
            child: Image.asset(
              image,
              height: screenSize.height * 7 / 100,
            ),
          ),
          SizedBox(
            height: screenSize.height * 1 / 100,
          ),
          Text(
            title,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: screenSize.height * 2.5 / 100),
          ),
          SizedBox(
            height: screenSize.height * .5 / 100,
          ),
          Text(
            percent,
            style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.w600,
                fontSize: screenSize.height * 2 / 100),
          )
        ],
      ),
    ));
  }
}
