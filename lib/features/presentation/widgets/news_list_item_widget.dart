import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/features/data/models/news_model.dart';
import 'package:innosoft_flutter_task/features/presentation/screens/MainScreens/details_screen.dart';

class NewsListItemWidget extends StatelessWidget {
  final NewsModel newsModel;

  const NewsListItemWidget({Key key, this.newsModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return FlatButton(
      padding: EdgeInsets.all(0),
      onPressed: (){
        Navigator.of(context).pushNamed(DetailsScreen.routeName,arguments: newsModel);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: screenSize.height * 1/100,horizontal:screenSize.width * 2.5/100 ),
        child: Row(
          children: <Widget>[
            // Image
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    newsModel.newsImage,
                    width: screenSize.width * 33 / 100,
                      height: screenSize.height * 16 / 100,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    right: 0,
                    bottom: 0,
                    child: Image.asset('assets/images/logo_badg.png'),
                    width: screenSize.width * 12 / 100,
                  )
                ],
              ),
            ),

            SizedBox(
              width: screenSize.width * 2 / 100,
            ),

            // Caontent
            Container(
              width: screenSize.width * 60 /100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    newsModel.newsCategory,
                    style: TextStyle(fontSize: screenSize.height * 2 / 100,color: Colors.grey),
                  ),
                  Text(
                    newsModel.newsTitle,
                    maxLines: 2,
                    style: TextStyle(
                        color: Colors.black, fontSize: screenSize.height * 2.2 / 100,fontWeight: FontWeight.bold),
                  ),
                  Text(
                    newsModel.newsDate,
                    style: TextStyle(fontSize: screenSize.height * 2 / 100,color: Colors.grey),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
