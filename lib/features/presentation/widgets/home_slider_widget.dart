import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeSliderWidget extends StatelessWidget {

  final String sliderImage;


  const HomeSliderWidget({Key key,
    this.sliderImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery
        .of(context)
        .size;

    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Stack(
        children: <Widget>[
          Image.asset(
            sliderImage,
            width: screenSize.width * 95 / 100,
            height: screenSize.height * 30 / 100,
            fit: BoxFit.cover,
          ),
          Positioned(
            right: 0,
            bottom: 0,
            child: Image.asset('assets/images/logo_badg.png'),
            width: screenSize.width * 18 / 100,
          )
        ],
      ),
    );
  }
}
