import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/features/data/models/users_model.dart';
import 'package:innosoft_flutter_task/features/domain/usecases/users_data_usecase.dart';

class UsersProvider extends ChangeNotifier{

  Future<UsersModel> getUsersData() async => await UsersDataUsercase().getUsersData();

}