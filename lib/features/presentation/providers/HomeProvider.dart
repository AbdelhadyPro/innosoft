
import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/features/data/models/match_model.dart';
import 'package:innosoft_flutter_task/features/data/models/twitter_model.dart';
import 'package:innosoft_flutter_task/features/data/models/videos_model.dart';
import 'package:innosoft_flutter_task/features/data/models/winner_model.dart';
import 'package:innosoft_flutter_task/features/domain/usecases/home_usecase.dart';

class HomeProvider extends ChangeNotifier{

  List<String> getHomeSliderList()  => HomeUsercase().getHomeSliderList();

  List<MatchModel> getMatchesList() => HomeUsercase().getMatchesList();

  List<TwitterModel> getTwitterList() => HomeUsercase().getTwitterList();

  List<WinnerModel> getWinnerList() => HomeUsercase().getWinnerList();

  List<VideosModel> getVideosList() => HomeUsercase().getVideosList();

}