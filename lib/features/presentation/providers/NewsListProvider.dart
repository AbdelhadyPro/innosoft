import 'package:flutter/cupertino.dart';
import 'package:innosoft_flutter_task/features/data/models/news_model.dart';
import 'package:innosoft_flutter_task/features/domain/usecases/news_list_usecase.dart';

class NewsListProvider extends ChangeNotifier{

  List<NewsModel> getNewsList() {
    return NewsListUsercase().getNewsListData();
  }
}