import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/core/app_colors.dart';
import 'package:innosoft_flutter_task/core/localization/app_translationss.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/more_btn_widget.dart';

class MoreScreen extends StatelessWidget {
  // Navigator route name
  static final String routeName = '/more-screen';

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: darkBlueColor,
      body: Container(
        child: ListView(
          children: <Widget>[
            // logo
            Container(
              height: screenSize.height * 13 / 100,
              child: Image.asset('assets/images/logo.png'),
              padding: EdgeInsets.all(screenSize.height * 3 / 100),
              alignment: Alignment.centerRight,
            ),

            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx1'),
                color: Colors.white,
                onClick: () {}),
            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx2'),
                color: Colors.white,
                onClick: () {}),
            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx3'),
                color: Colors.red,
                onClick: () {}),
            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx4'),
                color: Colors.green,
                onClick: () {}),
            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx5'),
                color: Colors.blue,
                onClick: () {}),
            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx6'),
                color: Colors.white,
                onClick: () {}),
            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx7'),
                color: Colors.red,
                onClick: () {}),
            MoreBtnWidget(
                screenSize: screenSize,
                title: AppTranslations.of(context).text('key_more_tx8'),
                color: Colors.green,
                onClick: () {}),

            // Social
            Container(
              margin: EdgeInsets.symmetric(horizontal: screenSize.width * 7/100),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Image.asset('assets/images/fb.png',width: screenSize.height * 3/100,height: screenSize.height * 3/100,),
                  SizedBox(height: screenSize.height * 2/100,),
                  Image.asset('assets/images/tw.png',width: screenSize.height * 3/100,height: screenSize.height * 3/100,),
                  SizedBox(height: screenSize.height * 2/100,),
                  Image.asset('assets/images/insta.png',width: screenSize.height * 3/100,height: screenSize.height * 3/100,),
                  SizedBox(height: screenSize.height * 2/100,),
                  Image.asset('assets/images/in.png',width: screenSize.height * 3/100,height: screenSize.height * 3/100,),
                  SizedBox(height: screenSize.height * 2/100,),
                  Image.asset('assets/images/tube.png',width: screenSize.height * 3/100,height: screenSize.height * 3/100,),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
