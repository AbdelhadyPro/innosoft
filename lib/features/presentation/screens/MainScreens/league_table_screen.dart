import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/core/parse_from_hex_to_color.dart';
import 'package:innosoft_flutter_task/features/data/data_sources/http_source/http_users_data.dart';
import 'package:innosoft_flutter_task/features/data/models/users_model.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/users_widget.dart';

class LeagueTableScreen extends StatefulWidget {
  // Navigator route name
  static final String routeName = '/league-table-screen';

  @override
  _LeagueTableScreenState createState() => _LeagueTableScreenState();
}

class _LeagueTableScreenState extends State<LeagueTableScreen> {
  bool _isLoading = true;
  UsersModel usersModel;

  @override
  void initState() {
    // get Users Data
    getUsersData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: parseColor('#F3F3F3'),
        appBar: PreferredSize(
          child: Image.asset(
            'assets/images/header.png',
            width: double.infinity,
            fit: BoxFit.fill,
          ),
          preferredSize: Size.zero,
        ),
        body: (_isLoading)
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                itemBuilder: (ctx, index) {
                  final user = usersModel.data[index];
                  return Column(
                    children: <Widget>[
                      UsersWidget(
                        image: user.avatar,
                        firstname: user.firstName,
                        lastname: user.lastName,
                        email: user.email,
                      ),
                      (index == usersModel.data.length - 1)
                          ? SizedBox(height: screenSize.height * 2/100,)
                          : Container()
                    ],
                  );
                },
                itemCount: usersModel.data.length,
              ));
  }

  void getUsersData() {
    HttpUsersData().getData().then((value) {
      setState(() {
        _isLoading = false;
      });
      final usersMap = json.decode(value.body) as Map<String, dynamic>;
      final usersModel = UsersModel.fromJson(usersMap);
      this.usersModel = usersModel;
    });
  }
}
