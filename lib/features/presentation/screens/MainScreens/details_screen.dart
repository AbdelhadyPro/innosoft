import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:innosoft_flutter_task/core/parse_from_hex_to_color.dart';
import 'package:innosoft_flutter_task/features/data/models/news_model.dart';

class DetailsScreen extends StatelessWidget {
  // Navigator route name
  static final String routeName = '/details-screen';

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    final newsModel = ModalRoute.of(context).settings.arguments as NewsModel;

    return Scaffold(
      backgroundColor: parseColor('#F3F3F3'),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Image cover
            Stack(
              children: <Widget>[
                Image.asset(
                  newsModel.newsImage,
                  width: double.infinity,
                  height: screenSize.height * 45 / 100,
                  fit: BoxFit.cover,
                ),
                Padding(
                  padding: EdgeInsets.only(top: screenSize.height * 5 / 100),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton.icon(
                        padding: EdgeInsets.all(0),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          label: Text(''),
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          )),
                      FlatButton.icon(
                          padding: EdgeInsets.all(0),
                          onPressed: () {},
                          label: Text(''),
                          icon: Icon(
                            Icons.share,
                            color: Colors.white,
                          )),
                    ],
                  ),
                )
              ],
            ),

            SizedBox(height: screenSize.height * 2 / 100),

            Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: screenSize.width * 4 / 100),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    newsModel.newsTitle,
                    style: TextStyle(
                      fontSize: screenSize.height * 2.5 / 100,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(height: screenSize.height * 1 / 100),
                  Text(
                    newsModel.newsCategory,
                    style: TextStyle(fontSize: screenSize.height * 2.5 / 100,color: Colors.black54,fontWeight: FontWeight.w400),
                  ),
                  Text(
                    newsModel.newsDate,
                    style: TextStyle(fontSize: screenSize.height * 1.8 / 100,color: Colors.black54),
                  ),
                  SizedBox(height: screenSize.height * 2 / 100),
                  Text(
                    newsModel.details,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: screenSize.height * 1.8 / 100,
                        color: Colors.black),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
