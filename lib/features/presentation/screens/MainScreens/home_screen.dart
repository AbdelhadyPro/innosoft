import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/core/localization/app_translationss.dart';
import 'package:innosoft_flutter_task/core/parse_from_hex_to_color.dart';
import 'package:innosoft_flutter_task/features/presentation/providers/HomeProvider.dart';
import 'package:innosoft_flutter_task/features/presentation/providers/NewsListProvider.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/home_videos_widget.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/home_match_widget.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/home_slider_widget.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/home_twitter_widget.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/home_winner_widget.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/news_list_item_widget.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  // Navigator route name
  static final String routeName = '/home-screen';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _homeSliderLength = 0;
  int _homeVideosLength = 0;
  int _sliderIndex = 0;
  int _videosIndex = 0;

  @override
  void initState() {
    // start auto slider play
    sliderAutoPlay();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: parseColor('#F3F3F3'),
      appBar: PreferredSize(
        child: Image.asset(
          'assets/images/header.png',
          width: double.infinity,
          fit: BoxFit.fill,
        ),
        preferredSize: Size.zero,
      ),
      body: ListView(
        children: [
          // Header
          HeaderWidget(screenSize),

          // last news
          SliderWidget(context, screenSize),

          // Coming Match
          ComingMatchWidget(context, screenSize),

          // Last Twitter
          LastTwitterWidget(context, screenSize),

          // Winner
          WinnerWidget(context, screenSize),

          // Video
          VideoWidget(context, screenSize),

          // Sponsers
          SponsersWidget(context, screenSize),
        ],
      ),
    );
  }

  // Sponsers
  Widget SponsersWidget(BuildContext context, Size screenSize) {
    return Column(
      children: <Widget>[
        // head
        HeadOfSection(context, screenSize,
            AppTranslations.of(context).text('key_sponsers'),
            haveMore: false),

        // Videos
        Container(
          width: screenSize.width * 100 / 100,
          padding: EdgeInsets.symmetric(
            vertical: screenSize.height * 5 / 100,
            horizontal: screenSize.height * 3 / 100,
          ),
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(child: Image.asset('assets/images/sponser.png')),
              SizedBox(width: screenSize.height * 3/100,),
              Expanded(child: Image.asset('assets/images/sponser.png')),
            ],
          ),
        )
      ],
    );
  }

  // Video
  Widget VideoWidget(BuildContext context, Size screenSize) {
    return Column(
      children: <Widget>[
        // head
        HeadOfSection(
            context, screenSize, AppTranslations.of(context).text('key_videos'),
            haveMore: false),

        // Videos
        Container(
          width: screenSize.width * 100 / 100,
          padding: EdgeInsets.symmetric(vertical: screenSize.height * 5 / 100),
          color: Colors.white,
          child: Consumer<HomeProvider>(
            builder: (ctx, value, child) {
              _homeVideosLength = value.getVideosList().length;
              final video = value.getVideosList()[_videosIndex];
              return HomeVideosWidget(
                image: video.image,
                title: video.title,
              );
            },
          ),
        )
      ],
    );
  }

  // Winner
  Widget WinnerWidget(BuildContext context, Size screenSize) {
    return Column(
      children: <Widget>[
        // head
        HeadOfSection(context, screenSize,
            AppTranslations.of(context).text('key_ges_winner'),
            haveMore: false),

        // body
        Container(
          color: Colors.white,
          child: Consumer<HomeProvider>(
            builder: (ctx, value, child) {
              return Container(
                padding: EdgeInsets.symmetric(horizontal: screenSize.height*3/100, vertical: screenSize.height*4/100),
                height: MediaQuery.of(context).size.height * 35 / 100,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (ctx, index) {
                    final winner = value.getWinnerList()[index];
                    return HomeWinnerWidget(
                        image: winner.image,
                        title: winner.title,
                        percent: winner.percent);
                  },
                  itemCount: value.getWinnerList().length,
                ),
              );
            },
          ),
        )
      ],
    );
  }

  // Twitter
  Widget LastTwitterWidget(BuildContext context, Size screenSize) {
    return Column(
      children: <Widget>[
        // head
        HeadOfSection(context, screenSize,
            AppTranslations.of(context).text('key_last_twitters')),
        // body
        Container(
          color: Colors.white,
          child: Consumer<HomeProvider>(
            builder: (ctx, value, child) {
              return ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (ctx, index) {
                  final twitter = value.getTwitterList()[index];
                  return Column(
                    children: <Widget>[
                      HomeTwitterWidget(
                          image: twitter.image,
                          title: twitter.title,
                          accout: twitter.accout,
                          content: twitter.content),
                      (index < value.getTwitterList().length - 1)
                          ? Divider()
                          : Container()
                    ],
                  );
                },
                itemCount: value.getTwitterList().length,
                shrinkWrap: true,
              );
            },
          ),
        )
      ],
    );
  }

  // comming matches
  Widget ComingMatchWidget(BuildContext context, Size screenSize) {
    return Column(
      children: <Widget>[
        //head
        HeadOfSection(context, screenSize,
            AppTranslations.of(context).text('key_coming_match')),
        // body
        Container(
          padding: EdgeInsets.symmetric(vertical: screenSize.height * 2 / 100),
          color: Colors.white,
          child: Consumer<HomeProvider>(
            builder: (ctx, value, child) {
              return ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (ctx, index) {
                  final match = value.getMatchesList()[index];
                  return Column(
                    children: <Widget>[
                      HomeMatchWidget(
                          firstTeam: match.firstTeam,
                          secondTeam: match.secondTeam,
                          time: match.time,
                          date: match.date),
                      (index < value.getMatchesList().length - 1)
                          ? Divider()
                          : Container()
                    ],
                  );
                },
                itemCount: value.getMatchesList().length,
                shrinkWrap: true,
              );
            },
          ),
        )
      ],
    );
  }

  // Header Widget
  Stack HeaderWidget(Size screenSize) {
    return Stack(
      children: <Widget>[
        Image.asset(
          'assets/images/header.png',
          width: double.infinity,
        ),
        Positioned(
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            padding: EdgeInsets.all(screenSize.height * 4 / 100),
            child: Image.asset(
              'assets/images/logo.png',
            ),
          ),
        ),
      ],
    );
  }

  // Slider Widget
  Column SliderWidget(BuildContext context, Size screenSize) {
    return // last news
        Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        // head
        HeadOfSection(context, screenSize,
            AppTranslations.of(context).text('key_last_news')),

        // Images
        Consumer<HomeProvider>(
          builder: (ctx, value, child) {
            _homeSliderLength = value.getHomeSliderList().length;
            return HomeSliderWidget(
              sliderImage: value.getHomeSliderList()[_sliderIndex],
            );
          },
        ),

        // Content
        Container(
          width: screenSize.width * 95 / 100,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppTranslations.of(context).text('key_global_category'),
                style: TextStyle(
                    fontSize: screenSize.height * 2.5 / 100,
                    color: Colors.grey),
              ),
              Text(
                AppTranslations.of(context).text('key_global_title'),
                maxLines: 2,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: screenSize.height * 2.5 / 100,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // Head Of all Sections
  Row HeadOfSection(BuildContext context, Size screenSize, String title,
      {bool haveMore = true}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FlatButton(
          child: Text(title,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: screenSize.height * 2 / 100)),
        ),

        (haveMore) ? Container(
          width: screenSize.width * 20 / 100,
          child: FlatButton(
            child: Text(
              AppTranslations.of(context).text('key_more'),
              style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: screenSize.height * 2 / 100),
            ),
            onPressed: () {},
          ),
        ) : Container()
      ],
    );
  }

  // Slider Auto Player (Slider & video slider)
  sliderAutoPlay() {
    Future.delayed(Duration(seconds: 3), () {
      // play slider
      if (_sliderIndex < _homeSliderLength - 1) {
        setState(() {
          _sliderIndex++;
        });
      } else {
        setState(() {
          _sliderIndex = 0;
        });
      }

      // play video
      if (_videosIndex < _homeVideosLength - 1) {
        setState(() {
          _videosIndex++;
        });
      } else {
        setState(() {
          _videosIndex = 0;
        });
      }

      sliderAutoPlay();
    });
  }

  @override
  void dispose() {

    super.dispose();
  }
}
