import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/core/localization/app_translationss.dart';
import 'package:innosoft_flutter_task/core/parse_from_hex_to_color.dart';

class FullStatsScreen extends StatelessWidget {
  // Navigator route name
  static final String routeName = '/full-stats-screen';

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: parseColor('#F3F3F3'),
        appBar: PreferredSize(
          child: Image.asset(
            'assets/images/header.png',
            width: double.infinity,
            fit: BoxFit.fill,
          ),
          preferredSize: Size.zero,
        ),
        body: Center(
            child: Text(
          AppTranslations.of(context).text('key_no_data'),
        )));
  }

}
