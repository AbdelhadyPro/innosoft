import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/core/localization/app_translationss.dart';
import 'package:innosoft_flutter_task/features/presentation/providers/NewsListProvider.dart';
import 'package:innosoft_flutter_task/features/presentation/widgets/news_list_item_widget.dart';
import 'package:provider/provider.dart';

class NewsListScreen extends StatelessWidget {
  // Navigator route name
  static final String routeName = '/news-list-screen';

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    // tap Items
    final List<Tab> myTabs = <Tab>[
      Tab(text: AppTranslations.of(context).text('key_news'),),
      Tab(text: AppTranslations.of(context).text('key_photos')),
      Tab(text: AppTranslations.of(context).text('key_videos')),
    ];

    return DefaultTabController(
      length: myTabs.length,
      child: Scaffold(
        // TabBar Items
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenSize.height * 25 / 100),
          child: Stack(
            children: <Widget>[
              Image.asset(
                'assets/images/header.png',
                width: double.infinity,
              ),
              Positioned(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: EdgeInsets.all(screenSize.height * 6 / 100),
                  child: Image.asset(
                    'assets/images/logo.png',
                  ),
                ),
              ),
              Container(
                height: screenSize.height * 18 / 100,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height:screenSize.height * 5/100,
                      child: TabBar(
                        tabs: myTabs,
                        indicatorColor: Colors.white,
                        indicatorWeight: 5,
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Cairo'
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

        // Tab Body
        body: TabBarView(
          children: [
            Consumer<NewsListProvider>(
              builder: (_, v, child) {
                return CustomNewsListView(v);
              },
            ),
            Center(
              child: Text(
                AppTranslations.of(context).text('key_no_data'),
              ),
            ),
            Center(
              child: Text(
                AppTranslations.of(context).text('key_no_data'),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget CustomNewsListView(NewsListProvider v) {
    return ListView.builder(
        itemBuilder: (ctx, index) {
          final newsItem = v.getNewsList()[index];
          return NewsListItemWidget(newsModel: newsItem,);
        },
        itemCount: v.getNewsList().length);
  }
}
