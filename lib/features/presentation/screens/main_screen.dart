import 'package:flutter/material.dart';
import 'package:innosoft_flutter_task/core/app_colors.dart';
import 'package:innosoft_flutter_task/core/localization/app_translationss.dart';
import 'package:innosoft_flutter_task/features/presentation/screens/MainScreens/full_stats_screen.dart';
import 'package:innosoft_flutter_task/features/presentation/screens/MainScreens/home_screen.dart';

import 'MainScreens/league_table_screen.dart';
import 'MainScreens/more_screen.dart';
import 'MainScreens/news_list_screen.dart';

class MainScreen extends StatefulWidget {
  // Navigator route name
  static final String routeName = '/main-screen';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  // init selected screen
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    // screen Size
    final screenSize = MediaQuery.of(context).size;

    // Pages Screens
    final tabs = [
      HomeScreen(),
      LeagueTableScreen(),
      NewsListScreen(),
      FullStatsScreen(),
      MoreScreen(),
    ];

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        backgroundColor: darkBlueColor,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        selectedFontSize: screenSize.height * 1.6 / 100,
        unselectedFontSize: screenSize.height * 1.4 / 100,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        items: [
          BottomNavBar(
            0,
            screenSize,
            'assets/images/bnb1.png',
            AppTranslations.of(context).text('key_home'),
          ),
          BottomNavBar(
            1,
            screenSize,
            'assets/images/bnb2.png',
            AppTranslations.of(context).text('key_leagueـtable'),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.chrome_reader_mode,
              size: getIconSize(2, screenSize),
              color: getIconcolor(2),
            ),
            title: Text(
              AppTranslations.of(context).text('key_media_center'),
            ),
          ),
          BottomNavBar(
            3,
            screenSize,
            'assets/images/bnb4.png',
            AppTranslations.of(context).text('key_full_stats'),
          ),
          BottomNavBar(
            4,
            screenSize,
            'assets/images/bnb5.png',
            AppTranslations.of(context).text('key_more'),
          ),
        ],
      ),
      body: tabs[_selectedIndex],
    );
  }

  BottomNavigationBarItem BottomNavBar(index, screenSize, asset, label) {
    return BottomNavigationBarItem(
      icon: Image.asset(asset,
          width: getIconSize(index, screenSize),
          height: getIconSize(index, screenSize),
          color: getIconcolor(index)),
      title: Text(label),
    );
  }

  // to check size case
  double getIconSize(int index, screenSize) {
    if (index == _selectedIndex) {
      return screenSize.height * 4 / 100;
    } else {
      return screenSize.height * 3 / 100;
    }
  }

  // to check color case
  Color getIconcolor(int index) {
    if (index == _selectedIndex) {
      return Colors.white;
    } else {
      return Colors.grey;
    }
  }
}
