import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:innosoft_flutter_task/features/data/core/constant.dart';

class HttpUsersData{
  Future<Response> getData() {
    return http.get(Constant.getHttpUsersDataLink);
  }
}
