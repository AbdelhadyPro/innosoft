import 'package:innosoft_flutter_task/features/data/models/match_model.dart';
import 'package:innosoft_flutter_task/features/data/models/twitter_model.dart';
import 'package:innosoft_flutter_task/features/data/models/videos_model.dart';
import 'package:innosoft_flutter_task/features/data/models/winner_model.dart';

class HomeFakeData {

  final List<String> homeSliderList = [
    "assets/images/product1.png",
    "assets/images/product2.png",
    "assets/images/product3.png"
  ];

  final List<MatchModel> matchesList = [
    MatchModel('الاهلي','الزمالك','18:00','الخميس 16 يوليو'),
    MatchModel('الشرقية','سموحة','20:00','الخميس 18 يوليو'),
    MatchModel('ليفربول','مان ستي','22:00','الخميس 20 يوليو'),
  ];

  final List<TwitterModel> twitterList = [
    TwitterModel('assets/images/logo.png','الدوري الرياضي','@account','عندما يريد العالم أن ‪يتكلّم ‬ ، فهو يتحدّث بلغة يونيكود. '),
    TwitterModel('assets/images/logo.png','محمد عبدالهادي','@mohamed','لا إله إلا انت سبحانك')
  ];

  final List<WinnerModel> winnerList = [
    WinnerModel('assets/images/team_image.png','الاتحاد','30%'),
    WinnerModel('assets/images/team_image.png','الهلال','50%'),
    WinnerModel('assets/images/team_image.png','النهضه','20%'),
  ];

  final List<VideosModel> videosList = [
    VideosModel('assets/images/product1.png','الصحف العالمية تبرر تمديد عقد محمد صلاح'),
    VideosModel('assets/images/product2.png','الاتحاد يفوز علي الاهلي'),
    VideosModel('assets/images/product3.png','مصر تفوز بكأس العالم لأول مرة'),
  ];



  List<String> getHomeSliderList() => homeSliderList;

  List<MatchModel> getMatchesList() => matchesList;

  List<TwitterModel> getTwittersList() => twitterList;

  List<WinnerModel> getWinnerList() => winnerList;

  List<VideosModel> getVideosList() => videosList;

}
