
class NewsModel{
  final int newsId;
  final String newsTitle;
  final String newsCategory;
  final String newsImage;
  final String newsDate;
  final String details;


  NewsModel({
    this.newsId,
    this.newsTitle,
    this.newsCategory,
    this.newsImage,
    this.newsDate,
    this.details,
  });
}
