class VideosModel {
  final String image;
  final String title;

  VideosModel(this.image, this.title);
}