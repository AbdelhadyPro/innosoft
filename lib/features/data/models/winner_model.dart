class WinnerModel {
  final String image;
  final String title;
  final String percent;

  WinnerModel(this.image, this.title, this.percent);
}