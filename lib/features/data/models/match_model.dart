class MatchModel {
  final String firstTeam;
  final String secondTeam;
  final String time;
  final String date;

  MatchModel(this.firstTeam, this.secondTeam, this.time, this.date);
}