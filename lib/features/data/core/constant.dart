class Constant{

  static String _httpGetUsersDataLink = "https://reqres.in/api/users";

  static String get getHttpUsersDataLink => _httpGetUsersDataLink;

}