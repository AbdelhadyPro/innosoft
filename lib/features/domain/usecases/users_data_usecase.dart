import 'package:innosoft_flutter_task/features/data/models/users_model.dart';
import 'package:innosoft_flutter_task/features/domain/repositories/users_repo.dart';

class UsersDataUsercase {

  Future<UsersModel> getUsersData() async => await UsersRepo().getUsersData();
}