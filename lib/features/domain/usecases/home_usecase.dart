import 'package:innosoft_flutter_task/features/data/models/match_model.dart';
import 'package:innosoft_flutter_task/features/data/models/twitter_model.dart';
import 'package:innosoft_flutter_task/features/data/models/videos_model.dart';
import 'package:innosoft_flutter_task/features/data/models/winner_model.dart';
import 'package:innosoft_flutter_task/features/domain/repositories/home_reop.dart';

class HomeUsercase{

  List<String> getHomeSliderList() => HomeRepo().getHomeSliderList();

  List<MatchModel> getMatchesList() => HomeRepo().getMatchesList();

  List<TwitterModel> getTwitterList() => HomeRepo().getTwitterList();

  List<WinnerModel> getWinnerList() => HomeRepo().getWinnerList();

  List<VideosModel> getVideosList() => HomeRepo().getVideosList();


}