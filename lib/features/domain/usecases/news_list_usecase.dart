import 'package:innosoft_flutter_task/features/data/models/news_model.dart';
import 'package:innosoft_flutter_task/features/domain/repositories/news_repo.dart';

class NewsListUsercase{

  List<NewsModel> getNewsListData() => NewsRepo().getNewsList();

}