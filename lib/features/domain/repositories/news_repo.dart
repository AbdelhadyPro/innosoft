import 'package:innosoft_flutter_task/features/data/data_sources/fake_data/news_fake_data.dart';
import 'package:innosoft_flutter_task/features/data/models/news_model.dart';

class NewsRepo {
  List<NewsModel> getNewsList() => NewsFakeData().newsList;
}
