import 'dart:convert';
import 'package:innosoft_flutter_task/features/data/data_sources/http_source/http_users_data.dart';
import 'package:innosoft_flutter_task/features/data/models/users_model.dart';

class UsersRepo {
  // hande data in Model
  Future<UsersModel> getUsersData()  async {
    UsersModel usersModel;
     await HttpUsersData().getData().then((value) => (response) {
          final usersMap = json.decode(response.body) as Map<String, dynamic>;
          usersModel = UsersModel.fromJson(usersMap);
        });
    return usersModel;
  }
}
