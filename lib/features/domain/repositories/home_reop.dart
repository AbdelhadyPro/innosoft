import 'package:innosoft_flutter_task/features/data/data_sources/fake_data/home_fake_data.dart';
import 'package:innosoft_flutter_task/features/data/models/match_model.dart';
import 'package:innosoft_flutter_task/features/data/models/twitter_model.dart';
import 'package:innosoft_flutter_task/features/data/models/videos_model.dart';
import 'package:innosoft_flutter_task/features/data/models/winner_model.dart';

class HomeRepo {

  List<String> getHomeSliderList() => HomeFakeData().homeSliderList;

  List<MatchModel> getMatchesList() => HomeFakeData().getMatchesList();

  List<TwitterModel> getTwitterList() => HomeFakeData().getTwittersList();

  List<WinnerModel> getWinnerList() => HomeFakeData().getWinnerList();

  List<VideosModel> getVideosList() => HomeFakeData().getVideosList();

}